// This file is part of the didu source code.
//
// ©️ 2022 Anna Aurora <mailto:anna@annaaurora.eu> <https://matrix.to/#/@papojari:artemislena.eu> <https://annaaurora.eu>
//
// For the license information, please view the README.md file that was distributed with this source code.

use clap::Parser;
use unit_conversions::time;
use recolored::*;

#[derive(Parser, Debug)]
#[clap(name = "didu", version, about="Duration conversion between units")]
struct Args {
    /// Duration value
    #[clap(long, short = 'v')]
    input_value: f64,

    /// Duration unit
    #[clap(long, short = 'u', possible_values = ["s", "min", "h", "d", "w", "m", "y", "dec", "cen"], default_value = "s")]
    input_unit: String,

    /// What unit(s) to convert to. "all" is not a unit but means all units.
    #[clap(long, short = 'o', possible_values = ["s", "min", "h", "d", "w", "m", "y", "dec", "cen", "all"], default_value = "all")]
    output_unit: String,

    /// If the output should be just the result or also show the input.
    #[clap(long, short = 'r')]
    only_result: bool,

    /// Don't print the unit indicator on the duration in the output unit. Only works with the only-result option.
    #[clap(long, short = 'n')]
    no_unit: bool,

    /// Specify to which decimal place the duration in the output unit should be rounded to.
    #[clap(long, short = 'd', default_value = "2")]
    round: u8,
}

struct PrintOutputUnit {
    seconds: bool,
    minutes: bool,
    hours: bool,
    days: bool,
    weeks: bool,
    months: bool,
    years: bool,
    decades: bool,
    centuries: bool,
}

fn main() {
    let args = Args::parse();
    let input_value = args.input_value;
    let input_unit = args.input_unit;
    let output_unit = args.output_unit;
    let only_result = args.only_result;
    let no_unit = args.no_unit;
    let round = args.round;

    let minutes = match input_unit.as_str() {
        "s" => time::seconds::to_minutes(input_value),
        "min" => time::seconds::to_minutes(time::minutes::to_seconds(input_value)),
        "h" => time::hours::to_minutes(input_value),
        "d" => time::days::to_minutes(input_value),
        "w" => time::weeks::to_minutes(input_value),
        "m" => time::months::to_minutes(input_value),
        "y" => time::years::to_minutes(input_value),
        "dec" => time::decades::to_minutes(input_value),
        "cen" => time::centuries::to_minutes(input_value),
        _ => panic!(),
    };
    let seconds = time::minutes::to_seconds(minutes);
    let hours = time::minutes::to_hours(minutes);
    let days = time::minutes::to_days(minutes);
    let weeks = time::minutes::to_weeks(minutes);
    let months = time::minutes::to_months(minutes);
    let years = time::minutes::to_years(minutes);
    let decades = time::minutes::to_decades(minutes);
    let centuries = time::minutes::to_centuries(minutes);

    // Print the duration(s) with the input unit if the only-result option is false.
    if only_result == false {
        println!("{} {} {}", input_value.to_string().yellow().bold(), input_unit.green().bold(), "are:" );
    };

    let options = match output_unit.as_str() {
        "s" => PrintOutputUnit {
            seconds: true,
            minutes: false,
            hours: false,
            days: false,
            weeks: false,
            months: false,
            years: false,
            decades: false,
            centuries: false,
        },
        "min" => PrintOutputUnit {
            seconds: false,
            minutes: true,
            hours: false,
            days: false,
            weeks: false,
            months: false,
            years: false,
            decades: false,
            centuries: false,
        },
        "h" => PrintOutputUnit {
            seconds: false,
            minutes: false,
            hours: true,
            days: false,
            weeks: false,
            months: false,
            years: false,
            decades: false,
            centuries: false,
        },
        "d" => PrintOutputUnit {
            seconds: false,
            minutes: false,
            hours: false,
            days: true,
            weeks: false,
            months: false,
            years: false,
            decades: false,
            centuries: false,
        },
        "w" => PrintOutputUnit {
            seconds: false,
            minutes: false,
            hours: false,
            days: false,
            weeks: true,
            months: false,
            years: false,
            decades: false,
            centuries: false,
        },
        "m" => PrintOutputUnit {
            seconds: false,
            minutes: false,
            hours: false,
            days: false,
            weeks: false,
            months: true,
            years: false,
            decades: false,
            centuries: false,
        },
        "y" => PrintOutputUnit {
            seconds: false,
            minutes: false,
            hours: false,
            days: false,
            weeks: false,
            months: false,
            years: true,
            decades: false,
            centuries: false,
        },
        "dec" => PrintOutputUnit {
            seconds: false,
            minutes: false,
            hours: false,
            days: false,
            weeks: false,
            months: false,
            years: false,
            decades: true,
            centuries: false,
        },
        "cen" => PrintOutputUnit {
            seconds: false,
            minutes: false,
            hours: false,
            days: false,
            weeks: false,
            months: false,
            years: false,
            decades: false,
            centuries: true,
        },
        "all" => PrintOutputUnit {
            seconds: true,
            minutes: true,
            hours: true,
            days: true,
            weeks: true,
            months: true,
            years: true,
            decades: true,
            centuries: true,
        },
        _ => panic!("Invalid input: {output_unit}"),
    };

    // Print the duration in the output unit…
    let precision: usize = round as usize;
    if no_unit && only_result {
        // …with no unit indicator(s).
        if options.seconds {
            println!("{:.1$}", seconds, precision);
        }
        if options.minutes  {
            println!("{:.1$}", minutes, precision);
        }
        if options.hours  {
            println!("{:.1$}", hours, precision);
        }
        if options.days  {
            println!("{:.1$}", days, precision);
        }
        if options.weeks  {
            println!("{:.1$}", weeks, precision);
        }
        if options.months  {
            println!("{:.1$}", months, precision);
        }
        if options.years  {
            println!("{:.1$}", years, precision);
        }
        if options.decades  {
            println!("{:.1$}", decades, precision);
        }
        if options.centuries  {
            println!("{:.1$}", centuries, precision);
        }
    } else {
        // …with unit indicator(s).
        if options.seconds {
            println!("{:.1$} {2}", seconds, precision, "s".blue());
        }
        if options.minutes  {
            println!("{:.1$} {2}", minutes, precision, "min".blue());
        }
        if options.hours  {
            println!("{:.1$} {2}", hours, precision, "h".blue());
        }
        if options.days  {
            println!("{:.1$} {2}", days, precision, "d".blue());
        }
        if options.weeks  {
            println!("{:.1$} {2}", weeks, precision, "w".blue());
        }
        if options.months  {
            println!("{:.1$} {2}", months, precision, "m".blue());
        }
        if options.years  {
            println!("{:.1$} {2}", years, precision, "y".blue());
        }
        if options.decades  {
            println!("{:.1$} {2}", decades, precision, "dec".blue());
        }
        if options.centuries  {
            println!("{:.1$} {2}", centuries, precision, "cen".blue());
        }
    };
}
