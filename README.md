# didu 🦀 🕘

*didu* is an acronym for *duration in dis unit*. It means that it shall give you your input duration in your specified output unit.

didu is a command line utility and is written in the 🦀 Rust language. Therefore very fast.

## API

This program uses [Semantic Versioning](https://semver.org/). Therefore an API has to be defined:
The API or the means of interacting with this program are the the command line options and the command line output. Excluded are name, version and about in the `--help` and the coloring.

If units are removed a major version will be released. However if units are added then that is considered backwards compatible and a major version will not be released.

If something with the output numbers is fixed then there will also be no major release.

So if changes are made to the exceptions then there will be no major or minor release because they are not part of the API. Thus if your program interacts with this one and uses features outside of the API you may expect them to break in between major releases.

## License

©️ 2022 Anna Aurora <mailto:anna@annaaurora.eu> <https://matrix.to/#/@papojari:artemislena.eu> <https://annaaurora.eu>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License version 3 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License version 3 along with this program. If not, see <https://www.gnu.org/licenses/>.

